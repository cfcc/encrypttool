package org.cfcc.pwtool;
/*
 * Main.java
 *
 * Created on December 27, 2007, 11:38 AM
 *
 * Copyright 2007, 2008, 2009 Jakim Friant
 *
 * This file is part of ResetPassword.
 *
 * ResetPassword is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ResetPassword is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ResetPassword.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.cfcc.CryptoException;
import org.cfcc.utils.Crypto;

/**
 * Use this to create an encrypted password that can be stored in web.xml config
 * file for ResetPassword.
 * @author jfriant
 */
public class Main {

    /** Creates a new instance of Main */
    public Main() {
    }

    /** Print a usage message and exit the program */
    public static void usage() {
        System.err.println("usage: java PasswordUtil PASSWORD_TEXT");
        System.exit(1);
    }

    /**
     * Read the password from the input args and print the encrypted version.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            usage();
        }

        String password = args[0];
        String output = null;

        Crypto c = new Crypto();
        try {
            output = c.encrypt(password);
        } catch (CryptoException ex) {
            ex.printStackTrace();
        }

        System.out.println("Paste the following password in web.xml:\n\n" + output + "\n");
    }
}

Encrypt Tool
============

This is used to generate encrypted passwords for the properties files used in the ResetPassword project.

Building
--------

You can use either Ant or Maven to build the project.  At CFCC I use Nexus to hold the project dependencies and so
Maven can automatically find the correct packages.  For Ant you will need to modify the build.xml file and update the
paths for the other project files (ColleagueLdapTools, ColFiles, etc.).

Maven Example
-------------

`mvn clean package`

Ant Example
-----------

`ant create_run_jar`

Usage
-----

`java -jar target/EncryptTool-1.0-SNAPSHOT-jar-with-dependencies.jar PASSWORD_PLAINTEXT`

Dependencies
------------

Requires the ColleagueLdapTools jar to be able to create the correct encryption format. 